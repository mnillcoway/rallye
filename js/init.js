function loadPrefsAndAssets () {
    return loadSpritesList()
        .then(loadSpritesImages);
}

function loadLoadingScreen () {
    return new Promise(function(resolve,reject){
        //var loader = new PIXI.loaders.Loader();
        //loader.add('menuBackground', 'images/loading.jpg');
        //
        //loader.once('complete' , function(loader, resources) {
        //    console.log('Loading screen loaded');
        //    callback(PIXI.Sprite.fromFrame('images/loading.jpg'));
        //});
        //loader.load();
        resolve();
    })
}

function loadSpritesImages () {
    Game.getFont = function (px) {
        return px + "px Arial";
    };
    return new Promise(function(resolve, reject) {
        var loader = new PIXI.loaders.Loader(), rejected = false;

        for (var key in Game.sprites) {
            if (Game.sprites.hasOwnProperty(key))
                loader.add(Game.sprites[key].name, Game.sprites[key].src);
        }

        loader.once('complete', function (loader, resources) {
            if (!rejected)
                resolve();
        });

        loader.once('error', function (loader, resources) {
            reject();
        });

        loader.load();
    });
}

function loadSpritesList() {
    return new Promise(function(resolve, reject){
        Game.sprites['road'] = {name: 'road', src:'images/road.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['car'] = {name: 'car', src:'images/car.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['greenCar'] = {name: 'greenCar', src:'images/greenCar.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['particle'] = {name: 'particle', src:'images/particle.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['button'] = {name: 'button', src:'images/button.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['buttonPressed'] = {name: 'buttonPressed', src:'images/buttonPressed.png', anchor:new Vec2(0.5,0.5)};

        Game.sprites['coin1'] = {name: 'coin1', src:'images/coin1.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['coin2'] = {name: 'coin2', src:'images/coin2.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['coin3'] = {name: 'coin3', src:'images/coin3.png', anchor:new Vec2(0.5,0.5)};
        Game.sprites['coin4'] = {name: 'coin4', src:'images/coin4.png', anchor:new Vec2(0.5,0.5)};

        Game.animations['coin'] = {name: 'buttonPressed', sprites:['images/coin1.png', 'images/coin2.png', 'images/coin3.png', 'images/coin4.png', 'images/coin3.png', 'images/coin2.png'], playTime:5000, anchor:new Vec2(0.5,0.5)};
        resolve();
    })
}