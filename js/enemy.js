/**
 * Created by Mnill on 12.12.15.
 */

function Enemy () {
    this.position = new Vec2(25 + 45 + (myRandom(0,3))*90, - 128);

    this.width = 34;
    this.height = 46;

    this.speed = Game.enemySpeed;
    this.sprite = Game.pool.getSprite('greenCar');
    this.sprite.position = this.position;
    Game.lvl.mainContaner.addChild(this.sprite);
}

Enemy.prototype.update = function() {
    this.position.y += Game.lvl.player.speed + -1 * this.speed;
    if (this.position.y < -512 || this.position.y > Game.height + 128) {
        Game.lvl.objEnemy.delEl(this);
        Game.pool.pull(this.sprite);
    }
};