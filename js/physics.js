/**
 * Created by Mnill on 12.12.15.
 * @return {boolean}
 */

function AABBtoCircleCollision(position, width, heght, cPosition, radius) {
    var distance = position.clone().sub(cPosition);

    var center = position.clone().sub(distance.clone().normalize().multiply(distance.getLength()/2));
    if (Math.abs(distance.x) < radius && Math.abs(distance.y) < heght/2) {
        return center;
    } else if (Math.abs(distance.y) < radius && Math.abs(distance.x) < width/2) {
        return center;
    }
    return false;
}

function CircleToCircleCollision(fCirclePos, fRadius, sCirclePos, sRadius) {
    return fCirclePos.clone().sub(sCirclePos).getLength() < fRadius + sRadius;
}