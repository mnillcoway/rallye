/**
 * Created by Mnill on 12.12.15.
 */

function Gui(params) {
    this.sprite = params.sprite;

    this.x = params.x;
    this.y = params.y;

    this.width = params.width;
    this.height = params.height;

    this.position = new Vec2(this.x + this.width/2, this.y + this.height/2);

    this.onTouch = params.onTouch;
    this.hidden = false;

    this.lock = !params.noLock;

    if (params.init)
        params.init.call(this, params);
}

Gui.prototype.update  = function () {
    if ((this.animation != null)&&(this.animationIterator > 0)) {
        if (this.animationDelay > 0) {
            this.animationDelay--;
            return;
        }
        this.x += (this.animation.toX - this.animation.startX) * (this.animationRevers ? -1 : 1) / this.animation.iterations;
        this.y += (this.animation.toY - this.animation.startY) * (this.animationRevers ? -1 : 1) / this.animation.iterations;

        if (this.guiContainer)
            this.guiContainer.position.set(this.x + this.width/2, this.y + this.height/2);


        if ((--this.animationIterator == 0) && (this.animationCallback != null)) {
            this.animationCallback();
        }

        if (this.guiContainer)
            this.guiContainer.position.set(this.x + this.width / 2, this.y + this.height / 2);
    }
    if (this.onUpdate)
        this.onUpdate();
};

Gui.prototype.checkTouch  = function(touch){
    if ((!this.hidden)&&(touch.startPosition.x > this.x)&& (touch.startPosition.x<this.x+this.width) && (touch.startPosition.y > this.y )&& (touch.startPosition.y<this.y+this.height))
    {
        this.onTouch(touch);
        return true;
    }
    return false;
};

//is point locked by gui;
Gui.prototype.isLocked = function(vec) {
    if (this.lock) {
        if ((vec.x > this.x)&& (vec.x<this.x+this.width) && (vec.y > this.y )&& (vec.y<this.y+this.height))
            return true;
    }
    return false;
};

Gui.prototype.startAnimation = function(animation, callback, reverse, delay){
    if (!animation)
        return;
    if (!reverse){
        this.x = animation.startX;
        this.y = animation.startY;
    } else {
        this.x = animation.toX;
        this.y = animation.toY;
    }
    this.animationRevers = reverse || false;
    this.animationDelay = delay || 0;
    this.animation = animation;
    this.animationIterator = animation.iterations;
    this.animationCallback = callback || null;
    if (this.guiContainer)
        this.guiContainer.position.set(this.x + this.width/2, this.y + this.height/2);
};


Gui.prototype.show = function(){

};


Gui.prototype.hide = function(){

};