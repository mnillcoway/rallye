/**
 * Created by Mnill on 12.12.15.
 */

function Player() {

    this.position = new Vec2(Game.width/2, Game.height - 64);
    this.sprite = Game.pool.getSprite('car');

    this.sprite.position = this.position;

    Game.lvl.mainContaner.addChild(this.sprite);
    this.road = 2;

    this.queue = [];
    this.speed  = Game.carSpeed;

    this.animationInProgress = false;

    this.radius = 36;
    this.booms = [];
}

Player.prototype.update = function() {
    Game.lvl.objEnemy.forEach(function(enemy) {
        var collision = null;
        if ((collision = AABBtoCircleCollision(enemy.position, enemy.width, enemy.height, this.position, this.radius))) {
            if (this.speed > 0)
                Game.lvl.loseGui.show();
            this.speed = 0;
            enemy.speed = 0;

            var already = false;
            this.booms.forEach(function (b) {
                if (b.clone().sub(collision).getLength() < 5){
                    already = true;
                }
            }.bind(this));
            if (!already) {
                Game.AudioPool.get('explosion', 0.3).play();
                this.booms.push(collision);
                Game.lvl.objEffect.push(new BoomEffect(collision));
                console.log('collision');
            }
        }
    }.bind(this));

    Game.lvl.objBonus.forEach(function(bonus) {
        if ((CircleToCircleCollision(bonus.position, bonus.radius, this.position, this.radius))) {
            bonus.take();
        }
    }.bind(this));

    if (this.animationInProgress && this.speed > 0) {
        this.animation();
    } else if (this.queue.length > 0 && this.speed > 0) {
        Game.AudioPool.get('turn', 0.1).play();
        var direction = this.queue.shift();
        if (this.road + direction > 0 && this.road + direction < 4) {
            this.animationInProgress = true;
            this.road += direction;
        }
    }
};

Player.prototype.left = function() {
    if (this.queue.length < 2)
        this.queue.push(-1);
};

Player.prototype.right = function() {
    if (this.queue.length < 2)
        this.queue.push(1);
};

Player.prototype.animation = function () {
    this.position.x += (25 + 45 + (this.road-1)*90) > this.position.x ? 9 : -9;
    if (this.position.x == (25 + 45 + (this.road-1)*90)) {
        this.animationInProgress = false;
    }
};