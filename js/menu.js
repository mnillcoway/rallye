/**
 * Created by Mnill on 12.12.15.
 */

function loadMainGui () {
    return new Promise(function(resolve) {

        Game.guiList.start = {
            noLock:true,
            sprite : null,
            x: Game.width/2 -92,
            y: Game.height/2-40,
            width: 184,
            height: 80,
            onTouch: function(touch) {
            },
            update: function(){},
            init:function() {
                this.guiContainer = new PIXI.Container();
                this.sprite = Game.pool.getSprite('button');

                this.guiContainer.addChild(this.sprite);
                this.guiContainer.position = this.position;

                this.text = new PIXI.Text('PLAY', {
                    font: Game.getFont(36),
                    fill: "#FFFFFF"
                });
                this.text.anchor.set(0.5, 0.5);
                this.guiContainer.addChild(this.text);

                this.guiContainer.interactive = true;
                this.guiContainer.mousedown = this.guiContainer.touchstart = function () {
                    this.yestoched = true;
                    this.sprite = Game.pool.switchSprite(this.sprite, 'buttonPressed');
                }.bind(this);

                this.guiContainer.mouseup = this.guiContainer.touchend = this.guiContainer.mouseupoutside = this.guiContainer.touchendoutside =  function () {
                    if (this.yestoched) {
                        this.yestoched = false;
                        this.sprite = Game.pool.switchSprite(this.sprite, 'button');
                        this.hide();
                    }
                }.bind(this);

                this.hide = function () {
                    this.startAnimation({startX: Game.width/2 -92, toX: Game.width/2-92, startY: Game.height/2-40, toY: -80, iterations:5}, function(){
                        Game.isMenu = false;
                    })
                }
            }
        };

        Game.guiList.lose = {
            noLock:true,
            sprite : null,
            x: Game.width * 2,
            y: Game.height/2 - 40,
            width: 184,
            height: 80,
            onTouch: function(touch) {},
            update: function(){},
            init:function() {
                this.guiContainer = new PIXI.Container();
                this.sprite = Game.pool.getSprite('button');

                this.guiContainer.addChild(this.sprite);
                this.guiContainer.position = this.position;

                this.text = new PIXI.Text('PLAY', {
                    font: Game.getFont(36),
                    fill: "#FFFFFF"
                });
                this.text.anchor.set(0.5, 0.5);
                this.guiContainer.addChild(this.text);

                this.scoreText = new PIXI.Text('', {
                    font: Game.getFont(36),
                    fill: "#FFC769",
                    stroke: "#000000",
                    strokeThickness: 5
                });
                this.scoreText.anchor.set(0.5, 0.5);
                this.scoreText.position.set(0, -90);
                this.guiContainer.addChild(this.scoreText);

                this.players = new PIXI.Text('You best scores:145\r\n You position is\r\n50 of 90 Players\r\n', {
                    font: Game.getFont(24),
                    fill: "#FFC769",
                    stroke: "#000000",
                    align: 'center',
                    strokeThickness: 3
                });
                this.players.anchor.set(0.5, 0);
                this.players.position.set(0, 50);
                this.guiContainer.addChild(this.players);
                this.players.alpha = 0;

                this.guiContainer.interactive = true;
                this.guiContainer.mousedown = this.guiContainer.touchstart = function () {
                    this.yestoched = true;
                    this.sprite = Game.pool.switchSprite(this.sprite, 'buttonPressed');
                }.bind(this);

                this.guiContainer.mouseup = this.guiContainer.touchend = this.guiContainer.mouseupoutside = this.guiContainer.touchendoutside =  function () {
                    if (this.yestoched) {
                        this.yestoched = false;
                        this.sprite = Game.pool.switchSprite(this.sprite, 'button');
                        this.hide();
                    }
                }.bind(this);

                this.show = function () {
                    this.showed = true;
                    this.scoreText.text = 'SCORE:' + parseInt(Game.lvl.score);
                    this.scoreText.alpha = 0;
                    this.players.alpha = 0;
                    this.startAnimation({startX: Game.width*2, toX: Game.width/2 - 92, startY: Game.height/2 - 40, toY: Game.height/2 - 40, iterations:5}, function() {
                        this.scoreText.alpha = 1;
                    });
                    var xhr = new XMLHttpRequest();
                    xhr.open("GET", 'https://mnillstone.com/games/rallye/compo/api/?id=' + new Fingerprint().get() +'&score=' + parseInt(Game.lvl.score) + '&nocache='+Date.now() , true);
                    xhr.onreadystatechange = function() {//Call a function when the state changes.
                        if (xhr.readyState == 4) {
                            if (xhr.status == 200) {
                                try {
                                    var scores = JSON.parse(xhr.responseText);
                                    this.players.text = 'You best scores:' + scores.youBest + '\r\n You position is\r\n' + (scores.youPos + 1) +' of ' + scores.players+ ' Players\r\n' + 'Best one has ' + scores.top;
                                    this.players.alpha = 1;
                                } catch (e) {
                                }
                                console.log(xhr.responseText)
                            } else {
                            }
                        }
                    }.bind(this);
                    xhr.send();
                };

                this.hide = function () {
                    this.showed = false;
                    this.startAnimation({startX: Game.width/2 - 92, toX: Game.width/2 - 92, startY: Game.height/2 - 40, toY: -380, iterations:5}, function(){
                        Game.lvl.init();
                    })
                }
            }
        };

        Game.guiList.score = {
            noLock:true,
            sprite : null,
            x: 20,
            y: 2,
            width: 20,
            height: 20,
            onTouch: function(touch) {},
            update: function(){},
            init:function() {
                this.guiContainer = new PIXI.Container();
                this.guiContainer.position = this.position;

                this.score = 0;
                this.text = new PIXI.Text('score:'+ parseInt(Game.lvl.score), {
                    font: Game.getFont(18),
                    fill: "#FFFFFF"
                });
                this.text.anchor.set(0, 0.5);
                this.guiContainer.addChild(this.text);

                this.guiContainer.interactive = true;
                this.guiContainer.mousedown = this.guiContainer.touchstart = function () {
                    this.yestoched = true;
                    this.sprite = Game.pool.switchSprite(this.sprite, 'buttonPressed');
                }.bind(this);

                this.guiContainer.mouseup = this.guiContainer.touchend = this.guiContainer.mouseupoutside = this.guiContainer.touchendoutside =  function () {
                    if (this.yestoched) {
                        this.yestoched = false;
                        this.sprite = Game.pool.switchSprite(this.sprite, 'button');
                        this.hide();
                    }
                }.bind(this);

                this.hide = function () {
                    this.startAnimation({startX: 20, toX: 20, startY: 2, toY: -80, iterations:10}, function() {});
                };

                this.onUpdate = function() {
                    this.text.text = 'score:'+ parseInt(Game.lvl.score);
                };
            }
        };

        resolve();
    });
}