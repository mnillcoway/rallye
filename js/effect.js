/**
 * Created by Mnill on 12.12.15.
 */
var boomParticlesConfig = {
    "alpha": {
        "start": 0.62,
        "end": 0
    },
    "scale": {
        "start": 0.25,
        "end": 0.75,
        "minimumScaleMultiplier": 1
    },
    "color": {
        "start": "#fafa2d",
        "end": "#616161"
    },
    "speed": {
        "start": 6,
        "end": 7
    },
    "acceleration": {
        "x": 0,
        "y": 0
    },
    "startRotation": {
        "min": 0,
        "max": 360
    },
    "rotationSpeed": {
        "min": 50,
        "max": 50
    },
    "lifetime": {
        "min": 0.1,
        "max": 0.75
    },
    "blendMode": "normal",
    "frequency": 0.01,
    "emitterLifetime": -1,
    "maxParticles": 1000,
    "pos": {
        "x": 0,
        "y": 0
    },
    "addAtBack": false,
    "spawnType": "circle",
    "spawnCircle": {
        "x": 0,
        "y": 0,
        "r": 10
    }
};

function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

function  BoomEffect(position) {
    var config = clone(boomParticlesConfig);
    config.pos.x = position.x;
    config.pos.y = position.y;
    var textures = [PIXI.utils.TextureCache[Game.sprites['particle'].src]];
    this.particle = new cloudkid.Emitter(
        Game.lvl.effectContainer,
        textures,
        config
    );
}

BoomEffect.prototype.update = function () {
    this.particle.update(Game.cycleTickTime/1000);
};

function ScoreEffect(position, count) {
    this.scoreText = new PIXI.Text('+'+count, {
        font: Game.getFont(18),
        fill: "#FFC769",
        stroke: "#000000",
        strokeThickness: 1
    });
    this.scoreText.anchor.set(0.5, 0.5);
    this.scoreText.position = position.clone();
    Game.lvl.effectContainer.addChild(this.scoreText);
    this.timer = 0;
}

ScoreEffect.prototype.update = function () {
    this.scoreText.position.y -= 1;
    if (++this.timer > 30){
        Game.lvl.objEffect.delEl(this);
        Game.pool.pull(this.scoreText);
    }
};


