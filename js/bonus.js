/**
 * Created by Mnill on 13.12.15.
 */

function BonusCoin () {
    this.position = new Vec2(25 + 45 + (myRandom(0,3)) * 90, - 128);

    this.width = 34;
    this.height = 46;

    this.radius = 12;
    this.sprite = Game.pool.getAnimation('coin');
    this.sprite.position = this.position;
    Game.lvl.underEffectContainer.addChild(this.sprite);
}

BonusCoin.prototype.update = function() {
    this.position.y += Game.lvl.player.speed;
    if (this.position.y < -512 || this.position.y > Game.height + 128 || this.toDelete) {
        Game.lvl.objBonus.delEl(this);
        Game.pool.pull(this.sprite);
    }
};

BonusCoin.prototype.take = function() {
    this.toDelete = true;
    Game.AudioPool.get('coin', 0.2).play();
    Game.lvl.objEffect.push(new ScoreEffect(this.position, 5));
    Game.lvl.score += 5;
};