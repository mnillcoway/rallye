/**
 * Created by Mnill on 13.12.15.
 */

function MyBackAudio(fileName, volume, loop) {
    this.fileName = fileName;
    this.loop = loop;
    this.audio = [];
    this.volume = volume || 0.5;
    var audio = new Audio();
    audio.src ='audio/' + this.fileName + '.mp3';
    audio.volume = this.volume;
    audio.loop = this.loop;
    this.audio.push(audio);
}

MyBackAudio.prototype.play = function (){
    var toPlay = this.fileName;
    if (!this.loop) {
        for (var realI = 0; realI < 100; realI++) {
            if (!this.audio[realI]) {
                this.audio[realI] = new Audio();
                this.audio[realI].msAudioCategory = "GameEffects";
                this.audio[realI].src = 'audio/' + toPlay + '.mp3';
                this.audio[realI].volume = this.volume;
                this.audio[realI].play();
                break;
            } else if (this.audio[realI].paused) {
                if (this.audio[realI].currentTime > 0)
                    this.audio[realI].currentTime = 0;
                this.audio[realI].play();
                break;
            }
        }
    } else {
        this.audio[0].play();
    }
};

MyBackAudio.prototype.stop = function (){
    var toPlay = this.fileName;
    if (!this.loop) {
        for (var realI = 0; realI < this.audio.length; realI++) {
            if (!this.audio[realI].paused && this.audio[realI].currentTime > 0) {
                this.audio[realI].stop();
            }
        }
    } else {
        this.audio[0].stop();
    }
};

function AudioPool() {
    this.audios = {};
}

AudioPool.prototype.get = function (name, volume, loop) {
    var audio;
    if (this.audios[name]) {
        audio = this.audios[name];
    } else {
        audio = new MyBackAudio(name, volume, loop);
        this.audios[name] = audio;
    }
    return audio;
};
