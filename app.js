/**
 * Created by Mnill on 14.12.15.
 */
'use strict';
const fs = require('fs');
var scores;
try {
    scores = JSON.parse(fs.readFileSync(__dirname + '/scores.json', 'utf8'));
} catch (e){
    scores = [];
}

var server = require('http').createServer(function (request, response) {
    var url = require('url').parse(request.url, true), query = url.query;
    if ('id' in query && query.id.length > 0  && query.id.length < 100 && 'score' in query && !isNaN(parseInt(query.score)) && parseInt(query.score) >= 0 && parseInt(query.score) <9999999) {
        let id = query.id, score = parseInt(query.score);
        let you = scores.filter(function(obj){
            return obj.id == id;
        }).pop();
        if (you) {
            if (you.score < score)
            you.score = score;
        } else {
            you = {id:id, score:score};
            scores.push(you);
        }
        scores = scores.sort(function(a,b){ return b.score-a.score});
        var answer = {
            players: scores.length,
            top: scores[0].score,
            youBest: you.score,
            youPos: scores.indexOf(you)
        };
        try {
            fs.writeFileSync(__dirname + '/scores.json', JSON.stringify(scores)); // not atomic, but it is not database :-)
        } catch (e){console.log('save file error')}
        response.writeHead(200, {'Content-Type': 'application/json'}); response.end(JSON.stringify(answer));
    } else {
        response.writeHead(400, {'Content-Type': 'application/json'}); response.end(JSON.stringify({error:'Bad request'}));
    }
});
server.listen(8081);